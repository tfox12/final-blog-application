<?php 
require_once 'db_connector.php';
require_once 'showTopMenu.php';
$id = $_GET['id'];


//select the rest of the values from the database



if($connection) {
    $sql_statement = "SELECT * FROM `devotion_table` WHERE `id` = $id LIMIT 1";
    $result = mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $devotion_topic = $row['devotion_topic'];
            $devotion_title = $row['devotion_title'];
            $devotion_body = $row['devotion_body'];
        }
    } else {
        echo "there was an sql problem " . mysqli_error($connection);
    }
}
else {
    echo "error connecting " . mysqli_connect_error();
}
?>
<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>
<div class="row">
<div class="leftcolumn">
<div class="card">


 <h2>Edit Devotion <?php echo "With id #" . $id;?></h2>
<h5>(Change fields and submit)</h5>
 
 	<div class="fakeimg" style="height:200px;">
		<form action="processEditItem.php">
			<input type= "hidden" name= "id" value= "<?php $id;?>"></input>
 			<textarea rows="1" cols="50" name="devotionTopic" placeholder="Devotion Topic"><?php echo $devotion_topic?></textarea><br>
 			<textarea rows="5" cols="50" name="devotionTitle" placeholder="Devotion Title"><?php echo $devotion_title?></textarea><br>
 			<textarea rows="5" cols="50" name="devotionBody" placeholder="Devotion Body"><?php echo $devotion_body?></textarea><br>
 				<button type="submit">Submit Changes</button>
 		</form>
 	</div>
 	
</div>
</div>
</div>
</body>
</html>