<?php
require_once 'db_connector.php';
require_once 'showTopMenu.php';

$user_id= $_SESSION['userid'];


$searchForTopic = $_GET['devotionTopic'];
$searchForTitle = $_GET['devotionTitle'];
$searchForBody = $_GET['devotionBody'];

$sql_statement = "SELECT devotion_table.*, users_table.id AS user_id, users_table.user_name FROM `devotion_table` JOIN `users_table` ON users_table.id = devotion_table.users_table_id WHERE `devotion_topic` LIKE '%$searchForTopic%'
AND `devotion_title` LIKE '%$searchForTitle%' AND `devotion_body` LIKE '%$searchForBody%'";

// The section below will fetch data from the table so that users can find devotions they created

if ($connection) {
    $result= mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row= mysqli_fetch_assoc($result)) {
            
// The html below will display devotions and comments as well as allow the user to create a comment

?>
<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>

<form action = "processNewComment.php">
<input type = "hidden" name = "id" value = "<?php echo $row['id'];?>"></input>
<div class="row">
	<div class="leftcolumn">
		<div class="card">
			<h1><?php echo $row['devotion_topic'] . " [id #" . $row['id'] . "]";?></h1>
			<h5><?php echo "-" . $row['devotion_title'] . "- [Devotion by " . $row['user_name'] . "]";?></h5>
			<div class="fakeimg" style="height:200px;"><?php echo $row['devotion_body'];?></div>
		</div>
	</div>
</div><hr>
 

<div class="rightcolumn">
	<div class="card">
		<h2>Add Comment</h2>
		<textarea class= "fakeimg" name= "comments" placeholder="comments" rows="5" cols= "50"></textarea>
		<button type= "submit">Submit Comments</button>
	</div>
</div>
</form> 
<?php

//sql statement for comments and likes. This will display comments that users have made

$devotion_id = $row['id'];
$sql_statement_comments = "SELECT comments_table.*, users_table.*, comment_like.id 
    AS like_id 
    FROM `comments_table` 
    JOIN `users_table` 
    ON users_table.id = comments_table.users_table_id 
    LEFT JOIN comment_like 
    ON comments_table.id = comment_like.comment_id 
    AND comment_like.user_id = '$user_id' 
    WHERE `devotion_table_id` = '$devotion_id'";
$result_comments = mysqli_query($connection, $sql_statement_comments);

if ($result_comments) {
    while($row_comments = mysqli_fetch_array($result_comments)) {
        $hasLiked = $row_comments['like_id'] != NULL;
?>

<div class="row">
	<div class="leftcolumn">
		<div class="card" style= "width: 60%">
			<a href="processLike.php?id=<?php echo $devotion_id;?>&liked=<?php echo $hasLiked ? 1 : 0; ?>">Like</a>
			<h1><?php echo "Comment by " . $row_comments['user_name'];?></h1>
			<div class="fakeimg" style="height:150px;"><?php echo $row_comments['comment_text'];?></div>
		</div>
	</div>
</div>

<?php

// This will display comment replies that users have made

$sql_statement_replies = "SELECT * FROM `replies` JOIN `users_table` ON users_table.id = replies.user_id WHERE `devotion_table_id` = '$devotion_id'";
$result_replies = mysqli_query($connection, $sql_statement_replies);
if ($result_replies) {
    while( $row_replies = mysqli_fetch_array($result_replies)) {
        echo $row_replies['reply'] . "<br>";
        echo "comment made by user " . $row_replies['user_name'] . "<br>";
    }
}

?>

<form action = "processNewReply.php">
<input type = "hidden" name = "reply_id" value = "<?php echo $row['id'];?>"></input>
	<div class="rightcolumn">
		<div class="card" style= "height:150px">
			<h3>Reply to Comment</h3>
			<textarea class= "fakeimg" name= "reply" placeholder="reply" rows="1" cols= "50"></textarea>
			<button type= "submit">Submit Reply</button>
		</div>
	</div>
</form>

<?php

    }
}
        }
        
    } else {
        echo "Error with the SQL " . mysqli_error($connection);
    }
    
    
    } else {
        echo "Error connecting " . mysqli_connect_error();
    }

?>