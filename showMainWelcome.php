<?php 
require_once 'db_connector.php';

$sql_statement = "SELECT * FROM `devotion_table`";

if ($connection) {
    $result= mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row= mysqli_fetch_assoc($result)) {
?>

<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>


<div class="row">
<div class="leftcolumn">
	<div class="card">
		<h1><?php echo $row['devotion_topic'];?></h1> 
		<h5>-<?php echo $row['devotion_title']?>-</h5>
		<div class="fakeimg" style="height:200px;"><?php echo $row['devotion_body'];?></div>
	</div>
</div>

</div>

</body>
</html>
<?php
       }
        
    } else {
        echo "Error with the SQL " . mysqli_error($connection);
    }
    
    
} else {
    echo "Error connecting " . mysqli_connect_error();
}

?>