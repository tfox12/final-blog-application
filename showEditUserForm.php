<?php
require_once 'db_connector.php';
require_once 'showTopMenu.php';
$id = $_GET['id'];

//we know id
//select the rest of the values from the database



if($connection) {
    $sql_statement = "SELECT * FROM `users_table` WHERE `id` = $id LIMIT 1";
    $result = mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row = mysqli_fetch_assoc($result)) {
            $user_name = $row['user_name'];
            $user_password = $row['user_password'];
            $user_role = $row['role'];
        }
    } else {
        echo "there was an sql problem " . mysqli_error($connection);
    }
}
else {
    echo "error connecting " . mysqli_connect_error();
}
?>
<html>
<head>
<link rel="stylesheet" type= "text/css" href="myWelcomeDesign.css">
</head>
<body>
<div class="row">
<div class="leftcolumn">
<div class="card">


 <h2>Edit User <?php echo "With id #" . $id;?></h2>
<h5>(Change fields and submit)</h5>
 
 	<div class="fakeimg" style="height:200px;">
		<form action="processEditUser.php">
			<input type= "hidden" name= "id" value= "<?php echo $id;?>"></input>
 			<textarea rows="1" cols="50" name="userName" placeholder="Username"><?php echo $user_name?></textarea><br>
 			<textarea rows="5" cols="50" name="userPassword" placeholder="Password"><?php echo $user_password?></textarea><br>
 			<textarea rows="5" cols="50" name="userRole" placeholder="Role"><?php echo $user_role?></textarea><br>
 				<button type="submit">Submit Changes</button>
 		</form>
 	</div>
 	
</div>
</div>
</div>
</body>
</html>