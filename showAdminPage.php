<?php
session_start();
require_once 'showTopMenu.php';
require_once 'db_connector.php';

if ( $_SESSION['role'] != 'admin') {
    echo "Please login as an admin<br>";
    exit;
}

$sql_statement = "SELECT * FROM `devotion_table`";

// The section below will fetch data from the table so that users can find devotions they created

if ($connection) {
    $result= mysqli_query($connection, $sql_statement);
    if ($result) {
        while ($row= mysqli_fetch_assoc($result)) {
            echo "devotion ID " . $row['id'] . "<br>";
            echo "devotion topic " . $row['devotion_topic'] . "<br>";
            echo "devotion title " . $row['devotion_title'] . "<br>";
            echo "devotion body " . $row['devotion_body'] . "<br>";
            
            ?>
            <form action="processDeleteItem.php">
            	<input type="hidden" name="id" value="<?php echo $row['id']; ?>"></input>
            	<button type="submit">Delete</button>
            </form>
            
            <form action="showEditForm.php">
            	<input type="hidden" name="id" value="<?php echo $row['id']; ?>"></input>
            	<button type="submit">Edit</button>
            </form>
            <?php
            
        }
        
    } else {
        echo "Error with the SQL " . mysqli_error($connection);
    }
    
    
} else {
    echo "Error connecting " . mysqli_connect_error();
}

?>